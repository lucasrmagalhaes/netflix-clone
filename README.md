# NETFLIX CLONE
Recriando a Interface do Netflix utilizando HTML, CSS e JavaScript.

[![NETFLIX CLONE](https://i.imgur.com/QwNjA7a.jpg "NETFLIX CLONE")](https://i.imgur.com/QwNjA7a.jpg "NETFLIX CLONE")
------------

- Desafio prático realizado na plataforma [Digital Innovation One](https://web.digitalinnovation.one/home "Digital Innovation One"): [Recriando a Interface do Netflix](https://web.digitalinnovation.one/lab/recriando-a-interface-do-netflix/learning/2069ecdf-36d6-4ad7-87fd-dab5632e722e "Recriando a Interface do Netflix").
